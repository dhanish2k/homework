package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMinHandler200(t *testing.T) {
	var jsonStr = []byte(`{"numbers":[2,4,5.6,8],"quantifier":1}`)
	req := httptest.NewRequest(http.MethodPost, "/min", bytes.NewBuffer(jsonStr))
	w := httptest.NewRecorder()
	minHandler(w, req)
	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		t.Errorf("Assertion error in Min Request")
	}
	fmt.Println(string(body))
}

func TestMinHandlerBadRequest(t *testing.T) {

	var jsonStr = []byte(`{"numbers":[2,4,5.6,8],"quantifier":80}`)
	req := httptest.NewRequest(http.MethodPost, "/min", bytes.NewBuffer(jsonStr))
	w := httptest.NewRecorder()
	minHandler(w, req)
	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 400 && string(body) != "quantifier value is invalid: has to be >=1 and less than the number of numbers" {
		t.Errorf("Assertion error in handling bad Min Request")
	}
	fmt.Println(string(body))
}

func TestMedianHandler200(t *testing.T) {
	var jsonStr = []byte(`{"numbers":[2,4,5.6]}`)
	req := httptest.NewRequest(http.MethodPost, "/median", bytes.NewBuffer(jsonStr))
	w := httptest.NewRecorder()
	medHandler(w, req)
	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		t.Errorf("Assertion error in Median Request")
	}
	fmt.Println(string(body))
}

func TestPercentile200(t *testing.T) {
	var jsonStr = []byte(`{"numbers":[2,4,5.6],"quantifier":60}`)
	req := httptest.NewRequest(http.MethodPost, "/percentile", bytes.NewBuffer(jsonStr))
	w := httptest.NewRecorder()
	perHandler(w, req)
	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		t.Errorf("Assertion error in Percentile Request")
	}
	fmt.Println(string(body))
}

func TestPercentile400(t *testing.T) {
	var jsonStr = []byte(`{"numbers":[2,4,5.6]}`)
	req := httptest.NewRequest(http.MethodPost, "/percentile", bytes.NewBuffer(jsonStr))
	w := httptest.NewRecorder()
	perHandler(w, req)
	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 400 {
		t.Errorf("Assertion error in Percentile Request")
	}
	fmt.Println(string(body))
}
