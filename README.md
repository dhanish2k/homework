**For python-script to comapre versions, please look** [here](https://gitlab.com/dhanish2k/homework/-/tree/main/python_script)

# math-api

# Requirements
- Implement a web service with the following operations
- min : given list of numbers and a quantifier (how many) provides min number(s)
- max - given list of numbers and a quantifier (how many) provides max number(s)
- avg - given list of numbers calculates their average
- median - given list of numbers calculates their median
- percentile - given list of numbers and quantifier 'q', compute the qth percentile of the list
elements

# Assumptions
- The list of numbers can be integers/decimals, I will use float64
- The optional quamtifer for some operations is a positive integer

# Operations

## 1. /min
- **type**: POST
- **conditions**: quantifier has to be >=1 and less than the number of numbers

<details><summary>Click to expand</summary>

- **sample payload**:
```json
{
    "numbers": [
        2,4,5.6
    ],"quantifier":1
}
```

**sample result**:
```json
{
    "operation": "min",
    "result": [
        2
    ]
}
```
</details>


## 2. /max
- **type**: POST
- **conditions**: quantifier has to be >=1 and less than the number of numbers

<details><summary>Click to expand</summary>

- **sample payload**:
```json
{
    "numbers": [
        2,4,5.6
    ],"quantifier":2
}
```

**sample result**:
```json
{
    "operation": "max",
    "result": [
        4,5.6
    ]
}
```
</details>

## 3. /avg
- **type**: POST

<details><summary>Click to expand</summary>

- **sample payload**:
```json
{
    "numbers": [
        2,4,5.6
    ]
}
```

**sample result**:
```json
{
    "operation": "avg",
    "result": [
        3.8666666666666667
    ]
}
```
</details>

## 4. /median
- **type**: POST

<details><summary>Click to expand</summary>

- **sample payload**:
```json
{
    "numbers": [
        2,4,5.6
    ]
}
```

**sample result**:
```json
{
    "operation": "median",
    "result": [
        4
    ]
}
```
</details>

## 5. /percentile
- **type**: POST
- **conditions**: quantifier value is invalid: has to be >=0 and <=100

<details><summary>Click to expand</summary>

- **sample payload**:
```json
{
    "numbers": [
        2,4,5.6
    ],"quantifier":80
}
```

**sample result**:
```json
{
    "operation": "percentile",
    "result": [
        5.6
    ]
}
```
</details>


# Testing



## Testing mathlibrary
```bash
cd homework/mathlib
$ go test -v
=== RUN   TestMin
--- PASS: TestMin (0.00s)
=== RUN   TestMax
--- PASS: TestMax (0.00s)
=== RUN   TestAvg
--- PASS: TestAvg (0.00s)
=== RUN   TestMedian
--- PASS: TestMedian (0.00s)
=== RUN   TestPercentile
--- PASS: TestPercentile (0.00s)
PASS
ok      homework/mathlib   0.228s
```


## Testing the Service
```bash
cd homework
$ go test -v
=== RUN   TestMinHandler200
{"operation":"min","result":[2]}
--- PASS: TestMinHandler200 (0.00s)
=== RUN   TestMinHandlerBadRequest
quantifier value is invalid: has to be >=1 and less than the number of numbers
--- PASS: TestMinHandlerBadRequest (0.00s)
=== RUN   TestMedianHandler200
{"operation":"median","result":[4]}
--- PASS: TestMedianHandler200 (0.00s)
=== RUN   TestPercentile200
{"operation":"percentile","result":[4]}
--- PASS: TestPercentile200 (0.00s)
=== RUN   TestPercentile400
quantifier value is invalid: has to be >=0 and <=100
--- PASS: TestPercentile400 (0.00s)
PASS
ok      homework   0.332s
```

# Things to do/ improve & general observations

- I wasn't sure how best to represent the datatypes, I went with float64 it was straightforward but that brought some quirks with floating point arithmetic. If it had been a production application, I'd spend more time to figure out how to represent them and imrprove the accuracy of some operations (eg: percentile).
- A bit of performance testing will go a long way in establishing the robustness of the application, especially when trying out a new programming language like myself. The tool of my choice is https://github.com/tsenart/vegeta
- Modularizing the applicaiton - I might explore how to modularize the applicaiton and make the code a bit more DRY. I read some articles around middlewares that might handle common opertions in api suchs as this such as logging & monitoring, error handling but decided not to proceed as I do not yet completey understand some finer aspects of Go.
- The python script was straightforward and it took me around 40 minutes to complete but the go web service took me longer than the stated time of 2 hours (approx 4 hours). Some of that time was also spent on deciding on how to organize the workspace and looking at best practices regarding packages and testing code. I'd hope that with  good books & tutorials, some holidays(near a beach:)) and a laptop, it should improve. 

