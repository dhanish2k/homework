package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"gitlab.com/dhanish2k/homework/mathlib"
)

type Input struct {
	Numbers    []float64 `json:"numbers"`
	Quantifier int       `json:"quantifier"`
}

type Output struct {
	Operation string    `json:"operation"`
	Result    []float64 `json:"result"`
}

// - min : given list of numbers and a quantifier (how many) provides min number(s)
// - max - given list of numbers and a quantifier (how many) provides max number(s)
// - avg - given list of numbers calculates their average
// - median - given list of numbers calculates their median
// - percentile - given list of numbers and quantifier 'q', compute the qth percentile of the list

func minHandler(w http.ResponseWriter, r *http.Request) {
	var input Input
	if e := handleInput(w, r, &input); e != nil {
		return
	}
	var result []float64
	if input.Quantifier < 1 || input.Quantifier > len(input.Numbers) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("quantifier value is invalid: has to be >=1 and less than the number of numbers"))
		return
	}
	result = mathlib.Min(input.Numbers, input.Quantifier)
	if e := handleOutput(w, "min", result); e != nil {
		return
	}
}

func maxHandler(w http.ResponseWriter, r *http.Request) {
	var input Input
	if e := handleInput(w, r, &input); e != nil {
		return
	}
	var result []float64
	if input.Quantifier < 1 || input.Quantifier > len(input.Numbers) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("quantifier value is invalid: has to be >=1 and less than the number of numbers"))
		return
	}
	result = mathlib.Max(input.Numbers, input.Quantifier)
	if e := handleOutput(w, "max", result); e != nil {
		return
	}
}

func avgHandler(w http.ResponseWriter, r *http.Request) {
	var input Input
	if e := handleInput(w, r, &input); e != nil {
		return
	}
	var result []float64
	result = mathlib.Avg(input.Numbers)
	if e := handleOutput(w, "avg", result); e != nil {
		return
	}
}

func medHandler(w http.ResponseWriter, r *http.Request) {
	var input Input
	if e := handleInput(w, r, &input); e != nil {
		return
	}
	var result []float64
	result = mathlib.Median(input.Numbers)
	if e := handleOutput(w, "median", result); e != nil {
		return
	}
}

func perHandler(w http.ResponseWriter, r *http.Request) {
	var input Input
	if e := handleInput(w, r, &input); e != nil {
		return
	}
	var result []float64
	if input.Quantifier <= 0 || input.Quantifier > 100 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("quantifier value is invalid: has to be >=0 and <=100"))
		return
	}
	result = mathlib.Percentile(input.Numbers, input.Quantifier)
	if e := handleOutput(w, "percentile", result); e != nil {
		return
	}
}

func handleInput(w http.ResponseWriter, r *http.Request, input *Input) error {
	payload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		errorString := "Error processing the input body"
		w.Write([]byte(errorString))
		return errors.New(errorString)
	}
	err = json.Unmarshal(payload, &input)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		errorString := "Error unmarshalling the input body"
		w.Write([]byte(errorString))
		return errors.New(errorString)
	}
	return nil
}

func handleOutput(w http.ResponseWriter, operation string, result []float64) error {
	output := &Output{
		Operation: operation,
		Result:    result}
	outputJson, err := json.Marshal(output)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		errorString := "Error marshalling the input body"
		return errors.New(errorString)
	}
	w.Header().Add("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(outputJson)
	return nil
}

func main() {

	http.HandleFunc("/min", minHandler)
	http.HandleFunc("/max", maxHandler)
	http.HandleFunc("/avg", avgHandler)
	http.HandleFunc("/median", medHandler)
	http.HandleFunc("/percentile", perHandler)
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		panic(err)
	}
}
