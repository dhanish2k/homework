package mathlib

import (
	"math"
	"sort"
)

func Min(numbers []float64, quantifier int) []float64 {
	sort.Float64s(numbers)
	return numbers[0:quantifier]
}

func Max(numbers []float64, quantifier int) []float64 {
	sort.Float64s(numbers)
	return numbers[len(numbers)-quantifier:]
}

func Avg(numbers []float64) []float64 {
	var total float64
	for i := 0; i < len(numbers); i++ {
		total += numbers[i]
	}
	//Adding the *100/100 to round to a floating point, will consider a different approach for better accuracy or dealing with bigger floats
	return []float64{(total / float64(len(numbers)) * 100) / 100}
}

func Median(numbers []float64) []float64 {
	sort.Float64s(numbers)
	mIndex := len(numbers) / 2
	if len(numbers)%2 != 0 {
		return []float64{numbers[mIndex]}
	} else {
		return []float64{((((numbers[mIndex-1]) + (numbers[mIndex])) / 2) * 100) / 100}
	}
}

func Percentile(numbers []float64, quantifier int) []float64 {
	sort.Float64s(numbers)
	// I've used the nearest rank method,https://en.wikipedia.org/wiki/Percentile#The_nearest-rank_method
	index := math.Ceil(float64(quantifier) * 0.01 * float64(len(numbers)))
	return []float64{numbers[int(index-1)]}
}
