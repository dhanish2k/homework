package mathlib

import (
	"reflect"
	"testing"
)

func TestMin(t *testing.T) {
	result := Min([]float64{2, 5.6, 3, 1.7}, 2)
	if !reflect.DeepEqual(result, []float64{1.7, 2}) {
		t.Errorf("Assertion error in Min")
	}
}

func TestMax(t *testing.T) {
	result := Max([]float64{2, 5.6, 3, 1.7}, 2)
	if !reflect.DeepEqual(result, []float64{3, 5.6}) {
		t.Errorf("Assertion error in Max")
	}
}

func TestAvg(t *testing.T) {
	result := Avg([]float64{2, 5.6, 3, 1.7})
	if !reflect.DeepEqual(result, []float64{3.075}) {
		t.Errorf("Assertion error in Avg")
	}
}

func TestMedian(t *testing.T) {
	result := Median([]float64{2, 5.6, 3, 1.7})
	if !reflect.DeepEqual(result, []float64{2.5}) {
		t.Errorf("Assertion error in Median")
	}
}

func TestPercentile(t *testing.T) {
	result := Percentile([]float64{2, 5.6, 3, 1.7}, 80)
	if !reflect.DeepEqual(result, []float64{5.6}) {
		t.Errorf("Assertion error in Min")
	}
}
