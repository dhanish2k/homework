from .version_compare import compare

#Example from the requirements
#0.1 < 1.1 < 1.2 < 1.2.9.9.9.9 < 1.3 < 1.3.4 <1.10

def test_version_compare_equality():
    assert compare("1.0","1.0") == 0
    assert compare("1.0.0.9","1.0.0.9") == 0


def test_version_compare():
    assert compare("1.1","0.1") == 1
    assert compare("1.2","1.1") == 1
    assert compare("1.2.9.9.9.9","1.2") == 1
    assert compare("1.3","1.2.9.9.9.9") == 1
    assert compare("1.3.4","1.3") == 1
    assert compare("1.10","1.3.4") == 1


def test_version_inverse():
    assert compare("0.1","1.1") == -1
    assert compare("1.1","1.2") == -1
    assert compare("1.2","1.2.9.9.9.9") == -1
    assert compare("1.2.9.9.9.9","1.3") == -1
    assert compare("1.3","1.3.4") == -1
    assert compare("1.3.4","1.10") == -1
    