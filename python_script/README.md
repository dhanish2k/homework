**_To use in your script_**
```python

from .version_compare import compare

#result = 1
result = compare("1.1","1.0")

#result = -1
result = compare("1.0","2.9.8")

#result = 0
result = compare("1.1","1.1")

```

**_To run tests_**

```bash

pip install -r requirements.txt

pytest -v

platform win32 -- Python 3.7.7, pytest-6.2.4, py-1.10.0, pluggy-0.13.1 -- c:\users\cloud\appdata\local\programs\python\python37\python.exe
cachedir: .pytest_cache
rootdir: C:\Users\cloud\go\src\homework\python_script
collected 3 items

test_version_compare.py::test_version_compare_equality PASSED                                                                                                                                                        [ 33%]
test_version_compare.py::test_version_compare PASSED                                                                                                                                                                 [ 66%]
test_version_compare.py::test_version_inverse PASSED     

```

