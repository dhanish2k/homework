def compare(version1, version2):

    #convert string versions to a list of integers
    version1_arr = list(map(int,version1.split(".")))
    print(version1_arr)
    version2_arr = list(map(int,version2.split(".")))
    print(version2_arr)

    for i in range(0,len(version1_arr),1):
        if(version1_arr[i]) >(version2_arr[i]):
            return 1
        elif((version2_arr[i])>(version1_arr[i])):
            return -1
        else:
            #If both the version have been equal so far

            #and no more elements in version1
            if(i==len(version1_arr)-1):
                #reached the last element in version2 as well
                if(i==len(version2_arr)-1):
                    return 0;
                # more elements in version2, so version2 is greater
                elif(i<len(version2_arr)-1):
                    return -1;
            #and no more elements in version 2
            if(i==len(version2_arr)-1):
                if(i==len(version1_arr)-1):
                    return 0;
                elif(i<len(version1_arr)-1):
                    return 1;
            continue
